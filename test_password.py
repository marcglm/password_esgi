import requests
# Demander à l'utilisateur de rentrer le mot de passe à vérifier
user_password = input("Veuillez entrer le mot de passe à vérifier: \n")

# Créer une fonction pour récupérer les 500 mots de passe populaires et comparer
def worst_500_passwords(password):
    #Download la SecList
    print("Téléchargement de la liste des 500 mots de passe...")
    url = 'https://raw.githubusercontent.com/danielmiessler/SecLists/master/Passwords/Common-Credentials/500-worst-passwords.txt'
    retrieve = requests.get(url)
    #Stocker le contenu dans la variable content
    content = retrieve.content

    if str(password) in str(content):
        print("Mot de passe correspond dans les 500 pires mots de passe... Changez-le immédiatement")
        exit(0)

def worst_10k_passwords(password):
    #Download la SecList 10K
    print("Téléchargement de la liste des 10000 mots de passe...")
    url = 'https://raw.githubusercontent.com/danielmiessler/SecLists/master/Passwords/Common-Credentials/10k-most-common.txt'
    retrieve = requests.get(url)
    #Stocker le contenu dans la variable content
    content = retrieve.content

    #Comparer le mot de passe à la liste
    if str(password) in str(content):
        print("Mot de passe correspond aux 10000 pires mots de passe... Changez-le immédiatement")
        exit(0)

if __name__ == "__main__":
    worst_500_passwords(user_password)
    worst_10k_passwords(user_password)
